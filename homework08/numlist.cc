#include "numlist.h"

// Partitioning functions
// Serial partition
unsigned int NumList::partition(vector<int>& keys, unsigned int low, 
                                unsigned int high)
{
    // Use the last element as the pivot
    int pivot = keys[high]; // index of the highest element
    int i = (low - 1);

    for (unsigned int j = low; j <= high - 1; j++) {
        if (keys[j] < pivot) {
            i++;
            // swap function
            int *a = &(keys[i]);
            int *b = &(keys[j]);
            int t = *a; 
            *a = *b;
            *b = t;
            // end swap
        }
    }
    // swap function 
    int *a = &(keys[i + 1]);
    int *b = &(keys[high]);
    int t = *a; 
    *a = *b;
    *b = t;
    // end swap

    return (i + 1);
}

// Parallel partition
unsigned int NumList:: partition_par(vector<int>& keys, unsigned int low,
                                     unsigned int high)
{
    // TODO: Implement the parallel partitioning method
    // There should be two #pragmas to parallelize the loop
    // First loop is calculating the lt and gt arrays
    // Second is when the integers are copied to the correct position (i.e.,
    // left or right side of the pivot

    // Use the last element as the pivot
    int pivot = keys[high]; // index of the highest element
    unsigned int n = (high - low + 1);

    if (n == 1) {   // if only one element in the vector, 
        return low; // return that element's index
    }

    // creating temp, greater than, and less than vectors with
    // n items all initialized to zero
    vector <int> b(n, 0); 
    vector <int> lt(n, 0);
    vector <int> gt(n, 0);

    // parallelizing for loop with static scheduler
    #pragma omp parallel for schedule(static)
    for (unsigned int i = 0; i < n-1; i++) {
        b[i] = keys[low+i];
        if (b[i] < pivot) {
            lt[i] = 1;
            gt[i] = 0;
        }
        else if (b[i] >= pivot) {
            gt[i] = 1;
            lt[i] = 0;
        }
    }
    // parallel prefix sum procedure
    for (unsigned int j = 1; j < n; j++) {
        lt[j] += lt[j-1];
        gt[j] += gt[j-1];
    }

    unsigned int k = low + lt[n-1];
    keys[k] = pivot;

    // parallelizing for loop with static scheduler
    // placing elements in correct position of keys vector
    #pragma omp parallel for schedule(static)
    for (unsigned int i = 0; i < n-1; i++) {
        if (b[i] < pivot) {
            keys[low + lt[i] - 1] = b[i];
        }
        else if (b[i] >= pivot) {
            keys[k + gt[i]] = b[i];
        }
    }
    return k; 
}

// Actual qsort that recursively calls itself with particular partitioning
// strategy to sort the list
void NumList::qsort(vector<int>& keys, int low, int high, ImplType opt)
{
    if(low < high) {
        unsigned int pi;
        if(opt == serial) {
            pi = partition(keys, low, high);
        } else {
            pi = partition_par(keys, low, high);
        }
        qsort(keys, low, pi - 1, opt);
        qsort(keys, pi + 1, high, opt);
    }
}

// wrapper for calling qsort
void NumList::my_qsort(ImplType opt)
{
    /* Initiate the quick sort from this function */
    qsort(list, 0, list.size() - 1, opt);
}
// Default constructor
// This should "create" an empty list
NumList::NumList() {
    /* do nothing */
    /* you will have an empty vector */
}
// Contructor
// Pass in a vector and the partitioning strategy to use
NumList::NumList(vector<int> in, ImplType opt) {
    list = in;
    my_qsort(opt);
}
// Destructor
NumList::~NumList() {
    /* do nothing */
    /* vector will be cleaned up automatically by its destructor */
}
// Get the element at index
int NumList::get_elem(unsigned int index)
{
    return list[index];
}
// Print the list
void NumList::print(ostream& os)
{
    for(unsigned int i = 0; i < list.size(); i++) {
        os << i << ":\t" << list[i] << endl;
    }
}

