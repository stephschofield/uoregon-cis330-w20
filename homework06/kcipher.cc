#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"

// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------

bool is_valid_alpha_RK(string alpha);
string delete_spaces(string alpha);

struct KCipher::KCipherCheshire {
	unsigned int index;
	vector<string> cont;
};

void KCipher::add_key(string key) {
	if (is_valid_alpha_RK(key)) {
		ksmile->cont.push_back(delete_spaces(key));
	}
	else {
		cout << "Invalid Running key: " << key << endl;
		exit(EXIT_FAILURE);
	}
};

void KCipher::set_id(unsigned int id) {
	if (0 <= id && id < ksmile->cont.size()) {
		ksmile->index = id;
	} else {
		cout << "Warning: invalid id: " << id << endl;
		exit(EXIT_FAILURE);
	}
};

KCipher::KCipher() {
	string key;
	for (int i = 0; i < MAX_LENGTH; i++) {
		key += 'a';
	}
	ksmile = new KCipherCheshire;
	ksmile->index = 0;
	ksmile->cont.push_back(key);
}

KCipher::KCipher(string key) {
	ksmile = new KCipherCheshire;
	ksmile->index = 0;
	add_key(key);
}

KCipher::~KCipher() {
	delete ksmile; 
}

string KCipher::encrypt(string enc) {
	string retStr;
	cout << "Encrypting...";
	// in is enc
	// running key is key

	for (int i = 0, j = 0; i < enc.size(); i++) {
		if (enc[i] == ' ') {
			retStr += enc[i];  
		}
		else {
			if ('a' <= enc[i] && enc[i] <= 'z') { // if it's lowercase
				int row = enc[i] - 'a';
				int rkey = ksmile->cont[ksmile->index][j] - 'a';
				// encryption process for lowercase
				retStr += ((26 + (row + rkey)) % 26) + 'a'; // don't need to check for +26 bc always positive but leaving for consistency
			}
			else if ('A' <= enc[i] && enc[i] <= 'Z') { 
				int row = enc[i] - 'A';
				int rkey = ksmile->cont[ksmile->index][j] - 'a'; // the key is  ALWAYS lowercase 
				// encryption process for uppercase
				retStr += ((26 + (row + rkey)) % 26) + 'A';
			}
			j++;
		}
	}

	cout << "Done" << endl;

	return retStr;
}

string KCipher::decrypt(string enc) {
	string retStr;
	cout << "Decrypting...";
	// in is enc
	// running key is key

	for (int i = 0, j = 0; i < enc.size(); i++) {
		if (enc[i] == ' ') {
			retStr += enc[i];  
		}
		else {
			if ('a' <= enc[i] && enc[i] <= 'z') { // if it's lowercase
				int row = enc[i] - 'a'; // resetting the index
				int rkey = ksmile->cont[ksmile->index][j] - 'a';
				// encryption process for lowercase
				retStr += ((26 + (row - rkey)) % 26) + 'a'; // don't need to check for +26 bc always positive but leaving for consistency
			}
			else if ('A' <= enc[i] && enc[i] <= 'Z') { 
				int row = enc[i] - 'A';
				int rkey = ksmile->cont[ksmile->index][j] - 'a'; // the key is  ALWAYS lowercase 
				// encryption process for uppercase
				retStr += ((26 + (row - rkey)) % 26) + 'A';
			}
			j++;
		}
	}

	cout << "Done" << endl;

	return retStr;
}

// decrypt = subtract letter in key from letter in tableau matrix to get letter in the "in"
// C, B, D, --> want B --> subtract C from D

string delete_spaces(string enc) {
	string retStr;
	for (int i = 0; i < enc.size(); i++) {
		if (enc[i] != ' ') {
			retStr += enc[i];
		}
	}
	return retStr;
}


bool is_valid_alpha_RK(string alpha)
{ // all lowercase, can be spaces
    bool is_valid = true;
    for(unsigned int i = 0; i < alpha.size(); i++) {
        char c = alpha[i];
	    if(! (((c >= 'a') && (c <= 'z')) || (c == ' '))) {
	        is_valid = false;
	    }
	}
    return is_valid;
}












