#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"
#include "vcipher.h"

// -------------------------------------------------------
// Vigenere Cipher implementation
// -------------------------------------------------------
bool is_valid_alpha_V(string alpha);

VCipher::VCipher() {
	// empty bc its inherited!!!!!
};

VCipher::~VCipher() {
	// empty bc its inherited!!!!!
};

VCipher::VCipher(string key) {
	string catstring;
	if (is_valid_alpha_V(key)) {
		for (int i = 0; i < MAX_LENGTH; i++) {
				catstring += key[i % key.size()];
		}
	} else {
		cout << "Error: not a valid Vigenere key word" << endl;
		exit(EXIT_FAILURE);
	}
	add_key(catstring);
	set_id(1);
}

bool is_valid_alpha_V(string alpha)
{ // no spaces, only lowercase
    bool is_valid = true;
    for(unsigned int i = 0; i < alpha.size(); i++) {
        char c = alpha[i];
	    if(! (c >= 'a') && (c <= 'z')) {
	        is_valid = false;
	    }
	} 
    return is_valid;
}
