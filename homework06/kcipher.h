#ifndef KCIPHER_H_
#define KCIPHER_H_
#include "cipher.h"
#include "ccipher.h"

using namespace std;

const unsigned int MAX_LENGTH = 100;

/* A class that implements a
   basic substitution cipher
 */

class KCipher : public Cipher {
protected: 
    struct KCipherCheshire;
    KCipherCheshire *ksmile;
public:
    KCipher();
    KCipher(string line);
    ~KCipher();
    virtual string encrypt(string raw);
    virtual string decrypt(string enc);
    virtual void add_key(string key);
    virtual void set_id(unsigned int id);
};

#endif


