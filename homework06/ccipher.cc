#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"

// -------------------------------------------------------
// Caesar Cipher implementation
// -------------------------------------------------------
struct Cipher::CipherCheshire {
    string cipher_alpha;
};

struct CCipher::CCipherCheshire {
    int rotation;
};

CCipher::CCipher() {
	csmile = new CCipherCheshire;
	csmile->rotation = 0;
}

CCipher::CCipher(int rot_num) {
	csmile = new CCipherCheshire;
	csmile->rotation = rot_num;
	rotate_string(Cipher::smile->cipher_alpha, rot_num);
}

CCipher::~CCipher() {
	delete csmile;
}

// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
	rotate(in_str.begin(), in_str.begin() + (rot % 26), in_str.end()); // rotate string in place
}




