#include "cipher.h"

/* Cheshire smile implementation.
   It only contains the cipher alphabet
 */
struct Cipher::CipherCheshire {
    string cipher_alpha;
};

/* This function checks the cipher alphabet
   to make sure it's valid
 */
bool is_valid_alpha(string alpha);


// -------------------------------------------------------
// Cipher implementation
/* Default constructor
   This will actually not encrypt the input text
   because it's using the unscrambled alphabet
 */
Cipher::Cipher()
{
    // TODO: Implement this default constructor 
    smile = new CipherCheshire;
    smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
}

/* This constructor initiates the object with a
   input cipher key
 */
Cipher::Cipher(string cipher_alpha)
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    if (is_valid_alpha(cipher_alpha))
      smile->cipher_alpha = cipher_alpha;
}

/* Destructor
 */
Cipher::~Cipher()
{
    delete smile;
}

/* This member function encrypts the input text 
   using the intialized cipher key
 */
string Cipher::encrypt(string raw)
{
    cout << "Encrypting...";
    string retStr;
    // TODO: Finish this function
    for (int i = 0; i < raw.size(); i++) { 
      if (raw[i] == ' ') { 
        retStr += ' ';
      }
      else if ('a' <= raw[i] && raw[i] <= 'z') { // if it's lowercase
        retStr += smile->cipher_alpha[raw[i] - 'a'];
      }
      else if ('A' <= raw[i] && raw[i] <= 'Z') { // if it's uppercase
        retStr += (smile->cipher_alpha[raw[i] - 'A'] - 'a') + 'A';
        //retStr[i] += 32; // make back to uppercase
      }

    }
    cout << "Done" << endl;

    return retStr;
}


/* This member function decrypts the input text 
   using the intialized cipher key
 */
string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    for (int i = 0; i < enc.size(); i++) { 
      if (enc[i] == ' ') { 
        retStr += ' ';
      }
      else if ('a' <= enc[i] && enc[i] <= 'z') { // if it's lowercase
        int index; // index equal to whatever pos returns
        index = find_pos(smile->cipher_alpha, enc[i]); // correct index
        retStr += index + 'a';
      }
      else if ('A' <= enc[i] && enc[i] <= 'Z') { // if it's uppercase
        int index; // index equal to whatever pos returns
        char lower = enc[i] + 32; // turning into lower
        index = find_pos(smile->cipher_alpha, lower); // correct index
        retStr += index + 'A';
      }

    }

    cout << "Done" << endl;

    return retStr;

}
// -------------------------------------------------------


//  Helper functions 
/* Find the character c's position in the cipher alphabet/key
 */
unsigned int find_pos(string alpha, char c)
{
    unsigned int pos = 0;

    for (int i = 0; i < alpha.size(); i++) {
      if (alpha[i] == c) {
        pos = i;
      }
    }

    return pos;
}

/* Make sure the cipher alphabet is valid - 
   a) it must contain every letter in the alphabet 
   b) it must contain only one of each letter 
   c) it must be all lower case letters 
   ALL of the above conditions must be met for the text to be a valid
   cipher alphabet.
 */
bool is_valid_alpha(string alpha)
{
    bool is_valid = true;
    if(alpha.size() != ALPHABET_SIZE) {
        is_valid = false; 
    } else {
        unsigned int letter_exists[ALPHABET_SIZE];
        for(unsigned int i = 0; i < ALPHABET_SIZE; i++) {
            letter_exists[i] = 0;
        }
        for(unsigned int i = 0; i < alpha.size(); i++) {
            char c = alpha[i];
            if(!((c >= 'a') && (c <= 'z'))) {
                is_valid = false;
            } else {
                letter_exists[(c - 'a')]++;
            }
        }
        for(unsigned int i = 0; i < ALPHABET_SIZE; i++) {
            if(letter_exists[i] != 1) {
                is_valid = false;
            }
        }
    }

    return is_valid;
}

