#include "bst.h"

// ---------------------------------------
// Node class
// Default constructor
Node::Node() {
// TODO: Implement this
	key = 0; 
	parent = NULL;
	left = NULL;
	right = NULL;
}
// Constructor
Node::Node(int in) {
// TODO: Implement this
	key = in;
	parent = NULL;
	left = NULL;
	right = NULL;
}
// Destructor
Node::~Node() {
// TODO: Implement this

}

// Add parent 
void Node::add_parent(Node* in) {
// TODO: Implement this
	parent = in;
}
// Add to left of current node
void Node::add_left(Node* in) {
// TODO: Implement this
	left = in;
}
// Add to right of current node
void Node::add_right(Node* in) {
// TODO: Implement this
	right = in;
}

// Get key
int Node::get_key()
{
// TODO: Implement this
	return key;
}
// Get parent node
Node* Node::get_parent()
{
// TODO: Implement this
	return parent;
}
// Get left node
Node* Node::get_left()
{
// TODO: Implement this
	return left;
}
// Get right node
Node* Node::get_right()
{
// TODO: Implement this
	return right;
}
// Print the key to ostream to
// Do not change this
void Node::print_info(ostream& to)
{
    to << key << endl;
}
// ---------------------------------------


// ---------------------------------------
// BST class
// Walk the subtree from the given node
void BST::inorder_walk(Node* in, ostream& to)
{
// TODO: Implement this
	if (in != NULL) {
		inorder_walk(in->get_left(), to);
		in->print_info(to);
		inorder_walk(in->get_right(), to);
	}
}
// Constructor
BST::BST()
{
// TODO: Implement this
	root = NULL;
}
// Destructor
BST::~BST()
{
// TODO: Implement this
	// while tree min !+ null
	// 		store tree min in m var
	// 		delete m
	// 	root = null

	while (tree_min() != NULL) {
		Node* m = tree_min();
		delete_node(m); 
	}
	root = NULL;
}
// Insert a node to the subtree
void BST::insert_node(Node* in)
{
// TODO: Implement this
	// z is in
	Node* y = NULL;
	Node* x = root;
	while (x != NULL) {
		y = x;
		if (in->get_key() < x->get_key()) {
			x = x->get_left();
		}
		else {
			x = x->get_right();
		}
	} 
	in->add_parent(y);
	if (y == NULL) {
		root = in;
	}
	else if (in->get_key() < y->get_key()) {
		// left[y] <- z
		y->add_left(in);
	}
	else {
		// right[y] <- z
		y->add_right(in);
	}
}
// Delete a node to the subtree
void BST::delete_node(Node* out)
{
// TODO: Implement this
	Node *u = NULL;
	Node *v = NULL;
	// ------ start delete ------
	// ----------- case 1 if statement ------------------
	if (out->get_left() == NULL) {
		// call transplant(newnode, newnode->get_right())
		// transplant(root1, root2)
		u = out;
		v = out->get_right();
		if (u->get_parent() == NULL) {
			root = v;
		}
		else if (u == (u->get_parent())->get_left()) {
			(u->get_parent())->add_left(v);
		}
		else {
			(u->get_parent())->add_right(v);		
		}
		if (v != NULL) {
			(v->add_parent(u->get_parent()));
		}
		out = u;
		out->add_right(v);
	}
	// ----------- case 2 else if statement ----------------
	else if (out->get_right() == NULL) {
		// call transplant(newnode, newnode->get_left())
		// transplant(root1, root2)
		u = out;
		v = out->get_left();
		if (u->get_parent() == NULL) {
			root = v;
		}
		else if (u == (u->get_parent())->get_left()) {
			(u->get_parent())->add_left(v);
		}
		else {
			(u->get_parent())->add_right(v);		
		}
		if (v != NULL) {
			(v->add_parent(u->get_parent()));
		}
		out = u;
		out->add_left(v);
	}
	// ----------- case 3 else statement -------------------
	else {
		Node *y = get_min(out->get_right());
		if (y->get_parent() != out) {
			// call transplant(successor, successor->get_right())
			Node *u = y;
			Node *v = y->get_right();
			if (u->get_parent() == NULL) {
			root = v;
			}
			else if (u == (u->get_parent())->get_left()) {
				(u->get_parent())->add_left(v);
			}
			else {
				(u->get_parent())->add_right(v);		
			}
			if (v != NULL) {
				(v->add_parent(u->get_parent()));
			}
			y = u;
			y->add_right(v);
			// ----------------- end transplant -------------------
			(y->add_right(out->get_right()));
			(y->get_right())->add_parent(y);
		}
		// -------- call transplant(newnode, successor) -------
		Node *u = out;
		Node *v = y;
		if (u->get_parent() == NULL) {
			root = v;
		}
		else if (u == (u->get_parent())->get_left()) {
			(u->get_parent())->add_left(v);
		}
		else {
			(u->get_parent())->add_right(v);		
		}
		if (v != NULL) {
			(v->add_parent(u->get_parent()));
		}
		out = u;
		y = v;
		// ----------------- end transplant -------------------
		y->add_left(out->get_left());
		(y->get_left())->add_parent(y);
	}
	// ------ end delete ------
	delete out;

}
// minimum key in the BST
Node* BST::tree_min()
{
// TODO: Implement this
	if (root == NULL) {
		return NULL;
	}
	else { 
		Node *temp = root;
		while (temp->get_left() != NULL) {
			temp = temp->get_left();
		}
		return temp;
	}
} 
// maximum key in the BST
Node* BST::tree_max()
{
// TODO: Implement this
	if (root == NULL) {
		return NULL;
	}
	else {
		Node *temp = root;
		while (temp->get_right() != NULL) {
			temp = temp->get_right();
		}
		return temp;
	}
}
// Get the minimum node from the subtree of given node
Node* BST::get_min(Node* in)
{
// TODO: Implement this
	if (in == NULL) {
		return NULL;
	}
	else { 
		Node *temp = in;
		while (temp->get_left() != NULL) {
			temp = temp->get_left();
		}
		return temp;
	}
}
// Get the maximum node from the subtree of given node
Node* BST::get_max(Node* in)
{
// TODO: Implement this
	if (in == NULL) {
		return NULL;
	}
	else {
		Node *temp = in;
		while (temp->get_right() != NULL) {
			temp = temp->get_right();
		}
		return temp;
	}
}
// Get successor of the given node
Node* BST::get_succ(Node* in)
{
// TODO: Implement this
	if (in->get_right() != NULL) {
		return get_min(in->get_right());
	}
	Node* y = in->get_parent();
	while (y != NULL && in == y->get_right()) {
		in = y;
		y = y->get_parent();
	}
	return y;
}
// Get predecessor of the given node
Node* BST::get_pred(Node* in)
{
// TODO: Implement this
	if (in->get_left() != NULL) {
		return get_max(in->get_left());
	}
	Node* y = in->get_parent();
	while (y != NULL && in == y->get_left()) {
		in = y;
		y = y->get_parent();
	}
	return y;
}
// Walk the BST from min to max
void BST::walk(ostream& to)
{
// TODO: Implement this
	inorder_walk(root, to);
}
// Search the tree for a given key
Node* BST::tree_search(int search_key)
{
// right[y] <- z
// y->add_right(in);
// TODO: Implement this
	Node *temp = root;
	while (temp != NULL && search_key != temp->get_key()) {
		if (search_key < temp->get_key()) {
			temp = temp->get_left();
		}
		else {
			temp = temp->get_right();
		}
	}
	return temp;
}
// ---------------------------------------









