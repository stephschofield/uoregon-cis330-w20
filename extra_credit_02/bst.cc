#include "bst.h"


// add insertfixup, deletefixup, transplant, SAME FUNCTION NAME
// ---------------------------------------
// Node class
// Default constructor
Node::Node() {
// TODO: Implement this
	key = 0; 
	color = 'r';
	parent = NULL;
	left = NULL;
	right = NULL;
}
// Constructor
Node::Node(int in) {
// TODO: Implement this
	key = in;
	color = 'r';
	parent = NULL;
	left = NULL;
	right = NULL;
}
// Destructor
Node::~Node() {
// TODO: Implement this

}
// Add parent 
void Node::add_parent(Node* in) {
// TODO: Implement this
	parent = in;
}
// Add to left of current node
void Node::add_left(Node* in) {
// TODO: Implement this
	left = in;
}
// Add to right of current node
void Node::add_right(Node* in) {
// TODO: Implement this
	right = in;
}
void Node::fix_color(char c) {
	color = c;
}
// Get key
int Node::get_key()
{
// TODO: Implement this
	return key;
}
// Get parent node
Node* Node::get_parent()
{
// TODO: Implement this
	return parent;
}
// Get left node
Node* Node::get_left()
{
// TODO: Implement this
	return left;
}
// Get right node
Node* Node::get_right()
{
// TODO: Implement this
	return right;
}
char Node::get_color() 
{
	return color;
}
// Print the key to ostream to
// Do not change this
void Node::print_info(ostream& to)
{
    to << key << endl;
}
// ---------------------------------------


// ---------------------------------------
// RBTree class
// Walk the subtree from the given node
void BST::inorder_walk(Node* in, ostream& to)
{
// TODO: Implement this
	if (in != NULL) {
		inorder_walk(in->get_left(), to);
		in->print_info(to);
		inorder_walk(in->get_right(), to);
	}
}
// Constructor
BST::BST()
{
// TODO: Implement this
	root = NULL;
	sentinel = NULL;
}
// Destructor
BST::~BST()
{
// TODO: Implement this
	// while tree min !+ null
	// 		store tree min in m var
	// 		delete m
	// 	root = null

	while (tree_min() != NULL) {
		Node* m = tree_min();
		delete_node(m); 
	}
	root = NULL;
	sentinel = NULL;
}
void BST::left_rotate(Node *n) {
	Node* y = n->get_right();
	n->add_right(y->get_left());
	if (y->get_left() != sentinel) {
		(y->get_left())->add_parent(n);
	}
	y->add_parent(n->get_parent());
	if (n->get_parent() == sentinel) {
		root = y;
	} else if (n == (n->get_parent()->get_left())) {
		(n->get_parent())->add_left(y);
	} else { 
		(n->get_parent())->add_right(y);
	}
	y->add_left(n);
	n->add_parent(y);
}

void BST::right_rotate(Node *n) {
	Node* y = n->get_left();
	n->add_left(y->get_right());
	if (y->get_right() != sentinel) {
		(y->get_right())->add_parent(n);
	}
	y->add_parent(n->get_parent());
	if (n->get_parent() == sentinel) {
		root = y;
	} else if (n == ((n->get_parent())->get_right())) {
		(n->get_parent())->add_right(y);
	} else { 
		(n->get_parent())->add_left(y);
	}
	y->add_right(n);
	n->add_parent(y);
}

// Insert a node to the subtree
void BST::rb_insert_fixup(Node *n) {
	Node *y;
	while ((n->get_parent())->get_color() == 'r') {
		if (n->get_parent() == (((n->get_parent())->get_parent())->get_left())) {
			y = ((n->get_parent())->get_parent())->get_right();
			// first case:
			if (y->get_color() == 'r') {
				(n->get_parent())->fix_color('b');
				y->fix_color('b');
				((n->get_parent())->get_parent())->fix_color('r');
				n = (n->get_parent())->get_parent();
			} else {
				if (n == (n->get_parent())->get_right()) {
						n = n->get_parent();
						left_rotate(n);
				}
				(n->get_parent())->fix_color('b');
				((n->get_parent())->get_parent())->fix_color('r');
				right_rotate((n->get_parent())->get_parent());
			}
		} else {
			y = ((n->get_parent())->get_parent())->get_left();
			// first case:
			if (y->get_color() == 'r') {
				(n->get_parent())->fix_color('b');
				y->fix_color('b');
				((n->get_parent())->get_parent())->fix_color('r');
				n = (n->get_parent())->get_parent();
			} else {
				if (n == (n->get_parent())->get_left()) {
					n = n->get_parent();
					left_rotate(n);
				} 
				(n->get_parent())->fix_color('b');
				((n->get_parent())->get_parent())->fix_color('r');
				left_rotate(n->get_parent()->get_parent());	
			}
		}
	}
	root->fix_color('b');
}

void BST::insert_node(Node *n) { // used to be RBTree::rb_insert
	Node *y = sentinel;
	Node *x = root;
	while (x != sentinel) {
		y = x;
		if (n->get_key() < x->get_key()) {
			x = x->get_left();
		}
		else {
			x = x->get_right();
		}
	}
	n->add_parent(y);
	if (y == sentinel) {
		root = n;
	} else if (n->get_key() < y->get_key()) {
		y->add_left(n); 
	} else {
		y->add_right(n);
	}
	n->add_left(sentinel);
	n->add_right(sentinel);
	n->fix_color('r');
	rb_insert_fixup(n);
}

void BST::rb_delete_fixup(Node *n) {
	Node *w;
	while ((n != root) && (n->get_color() == 'b')) {
		if (n == (n->get_parent())->get_left()) {
			w = (n->get_parent())->get_right();
			if (w->get_color() == 'r') {
				w->fix_color('b');
				(n->get_parent())->fix_color('r');
				left_rotate(n->get_parent());
				w = (n->get_parent())->get_right();
			}
			if (((w->get_left())->get_color() == 'b') && ((w->get_right())->get_color() == 'b')) {
				w->fix_color('r');
				n = n->get_parent();
			} else {
				if ((w->get_right())->get_color() == 'b') {
					(w->get_left())->fix_color('b');
					w->fix_color('r');
					right_rotate(w);
					w = (n->get_parent())->get_right();
				} 
				w->fix_color((n->get_parent())->get_color());
				(n->get_parent())->fix_color('b');
				(w->get_right())->fix_color('b');
				left_rotate(n->get_parent());
				n = root;
			}	
		} else {
			w = (n->get_parent())->get_left();
			if (w->get_color() == 'r') {
				w->fix_color('b');
				(n->get_parent())->fix_color('r');
				right_rotate(n->get_parent());
				w = (n->get_parent())->get_left();
			}
			if (((w->get_right())->get_color() == 'b') && ((w->get_left())->get_color() == 'b')) {
				w->fix_color('r');
				n = n->get_parent();
			} else {
				if ((w->get_left())->get_color() == 'b') {
					(w->get_right())->fix_color('b');
					w->fix_color('r');
					left_rotate(w);
					w = (n->get_parent())->get_left();
				} 
				w->fix_color((n->get_parent())->get_color());
				(n->get_parent())->fix_color('b');
				(w->get_left())->fix_color('b');
				right_rotate(n->get_parent());
				n = root;
			}	
		}
	}
	n->fix_color('b');
}

void BST::rb_transplant(Node* u, Node *v) {
	if (u->get_parent() == sentinel) {
		root = v;
	} else if (u == (u->get_parent())->get_left()) { // we can use Node public methods
		(u->get_parent())->add_left(v);
	} else {
		(u->get_parent())->add_right(v);
	}
	v->add_parent(u->get_parent());
}

void BST::delete_node(Node* n) { // used to be RBTree::rb_delete
	Node* y = n; Node* x;
	char y_orig_color = y->get_color();
	if (n->get_left() == sentinel) {
		x = n->get_right();
		rb_transplant(n, n->get_right());
	} else if (n->get_right() == sentinel) {
		x = n->get_left();
		rb_transplant(n, n->get_left());
	} else {
		y = get_min(n->get_right());
		y_orig_color = y->get_color();
		x = y->get_right();
		if (y->get_parent() == n) {
			x->add_parent(y);
		} else {
			rb_transplant(y, y->get_right());
			y->add_right(n->get_right());
			(y->get_right())->add_parent(y);
		}
		rb_transplant(n, y);
		y->add_left(n->get_left());
		(y->get_left())->add_parent(y);
		y->fix_color(n->get_color());
	}
	if (y_orig_color == 'b') {
		rb_delete_fixup(x);
	}
}

// minimum key in the BST
Node* BST::tree_min()
{
// TODO: Implement this
	if (root == NULL) {
		return NULL;
	}
	else { 
		Node *temp = root;
		while (temp->get_left() != NULL) {
			temp = temp->get_left();
		}
		return temp;
	}
} 
// maximum key in the BST
Node* BST::tree_max()
{
// TODO: Implement this
	if (root == NULL) {
		return NULL;
	}
	else {
		Node *temp = root;
		while (temp->get_right() != NULL) {
			temp = temp->get_right();
		}
		return temp;
	}
}
// Get the minimum node from the subtree of given node
Node* BST::get_min(Node* in)
{
// TODO: Implement this
	if (in == NULL) {
		return NULL;
	}
	else { 
		Node *temp = in;
		while (temp->get_left() != NULL) {
			temp = temp->get_left();
		}
		return temp;
	}
}

Node* BST::get_max(Node* in)
{
// TODO: Implement this
	if (in == NULL) {
		return NULL;
	}
	else {
		Node *temp = in;
		while (temp->get_right() != NULL) {
			temp = temp->get_right();
		}
		return temp;
	}
}
// Get successor of the given node
Node* BST::get_succ(Node* in)
{
// TODO: Implement this
	if (in->get_right() != NULL) {
		return get_min(in->get_right());
	}
	Node* y = in->get_parent();
	while (y != NULL && in == y->get_right()) {
		in = y;
		y = y->get_parent();
	}
	return y;
}
// Get predecessor of the given node
Node* BST::get_pred(Node* in)
{
// TODO: Implement this
	if (in->get_left() != NULL) {
		return get_max(in->get_left());
	}
	Node* y = in->get_parent();
	while (y != NULL && in == y->get_left()) {
		in = y;
		y = y->get_parent();
	}
	return y;
}
// Walk the BST from min to max
void BST::walk(ostream& to)
{
// TODO: Implement this
	inorder_walk(root, to);
}
// Search the tree for a given key
Node* BST::tree_search(int search_key)
{
// right[y] <- z
// y->add_right(in);
// TODO: Implement this
	Node *temp = root;
	while (temp != NULL && search_key != temp->get_key()) {
		if (search_key < temp->get_key()) {
			temp = temp->get_left();
		}
		else {
			temp = temp->get_right();
		}
	}
	return temp;
}
// ---------------------------------------









