#ifndef BST_H
#define BST_H
/* Red black tree classes
*
*
*/
#include <iostream>

using namespace std;

class Node {
private:
    int key;
    char color; // color = 'r'; color = 'b';
    Node* parent;
    Node* left;
    Node* right;
public:
    Node(); // default constructor
    Node(int data); // parameterized constructor
    virtual ~Node(); // virtual needed for inheritance

    void add_parent(Node* n);
    void add_left(Node* n);
    void add_right(Node* n);
    void fix_color(char c);

    int get_key();
    Node* get_parent();
    Node* get_left();
    Node* get_right();
    char get_color();

    virtual void print_info(ostream& to);
};

class BST {
private: 
    Node* root;
    Node* sentinel;

    void inorder_walk(Node* in, ostream& to);
    Node* get_min(Node* in);
    Node* get_max(Node* in);
public:
    BST(); // default constructor
    ~BST(); // destructor

    // RBT methods
    void left_rotate(Node *n);
    void right_rotate(Node *n);
    void rb_insert_fixup(Node *n);
    void rb_transplant(Node *u, Node *v);
    void rb_delete_fixup(Node *n);

    // BST methods
    void insert_node(Node *in);
    void delete_node(Node *out);
    Node* tree_min();
    Node* tree_max();
    Node* get_succ(Node* in); // successor for a given node
    Node* get_pred(Node* in); // predecessor for a given node
    void walk(ostream& to); // Traverse the tree from min to max recursively
    Node* tree_search(int search_key);
};


#endif








